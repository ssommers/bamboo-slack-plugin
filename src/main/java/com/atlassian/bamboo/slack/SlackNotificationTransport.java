package com.atlassian.bamboo.slack;

import com.atlassian.bamboo.author.Author;
import com.atlassian.bamboo.builder.BuildState;
import com.atlassian.bamboo.builder.LifeCycleState;
import com.atlassian.bamboo.deployments.results.DeploymentResult;
import com.atlassian.bamboo.notification.Notification;
import com.atlassian.bamboo.notification.NotificationTransport;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.resultsummary.ResultsSummary;
import com.atlassian.bamboo.utils.HttpUtils;
import com.atlassian.bamboo.variable.CustomVariableContext;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;

import org.apache.commons.lang.StringUtils;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.DefaultProxyRoutePlanner;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SlackNotificationTransport implements NotificationTransport
{
    private static final Logger log = Logger.getLogger(SlackNotificationTransport.class);

    // Can either be one of 'good', 'warning', 'danger', or any hex color code
    public static final String COLOR_UNKNOWN_STATE = "707070";
    public static final String COLOR_FAILED = "danger";
    public static final String COLOR_SUCCESSFUL = "good";
    public static final String COLOR_IN_PROGRESS = "warning";

    private final String webhookUrl;
    private final String channel;
    private final String from = "Bamboo";
    private final String iconUrl;

    private CloseableHttpClient client;

    @Nullable
    private final ImmutablePlan plan;
    @Nullable
    private final ResultsSummary resultsSummary;
    @Nullable
    private final DeploymentResult deploymentResult;

    public SlackNotificationTransport(String webhookUrl,
            String channel,
            String iconUrl,
            @Nullable ImmutablePlan plan,
            @Nullable ResultsSummary resultsSummary,
            @Nullable DeploymentResult deploymentResult,
            CustomVariableContext customVariableContext)
    {
        this.webhookUrl = customVariableContext.substituteString(webhookUrl);
        this.channel = customVariableContext.substituteString(channel);
        this.iconUrl = customVariableContext.substituteString(iconUrl);
        this.plan = plan;
        this.resultsSummary = resultsSummary;
        this.deploymentResult = deploymentResult;

        URI uri;
        try
        {
            uri = new URI(webhookUrl);
        }
        catch (URISyntaxException e)
        {
            log.error("Unable to set up proxy settings, invalid URI encountered: " + e);
            return;
        }

        HttpUtils.EndpointSpec proxyForScheme = HttpUtils.getProxyForScheme(uri.getScheme());
        if (proxyForScheme!=null)
        {
            HttpHost proxy = new HttpHost(proxyForScheme.host, proxyForScheme.port);
            DefaultProxyRoutePlanner routePlanner = new DefaultProxyRoutePlanner(proxy);
            this.client = HttpClients.custom().setRoutePlanner(routePlanner).build();
        }
        else
        {
            this.client = HttpClients.createDefault();
        }
    }

    public static String fallbackMessage(String message)
    {
        Document doc = Jsoup.parse(message);
        return escapeLinkText(doc.body().text().trim().replace("\u00a0", "")); //   "\u00a0" <=> &nbsp;
    }

    public static String textMessage(String message)
    {
        //log.error(message);
        Document doc = Jsoup.parse(message);
        Map<String, String> linkMap = new HashMap<String, String>();

        org.jsoup.select.Elements links = doc.getElementsByTag("a");
        for (org.jsoup.nodes.Element link : links) {
            String linkHref = link.attr("href");
            String linkText = link.text().replace("\u00a0", "").replace("+","\\+");
            linkMap.put(linkText, linkHref);
        }

        String resultMessage = doc.body().text().trim().replace("\u00a0", "");

        for(String key : linkMap.keySet())
        {
            Pattern keyPattern = Pattern.compile(key);
            Matcher keyMatcher = keyPattern.matcher(resultMessage);
            resultMessage = keyMatcher.replaceAll("<" + linkMap.get(key) + "|" + escapeLinkText(key) +">");
        }
        return resultMessage;
    }

    /**
     * Escape link text according to Slack Guidelines https://api.slack.com/docs/formatting#how_to_escape_characters
     *
     * @param key link content
     * @return escaped link content
     */
    private static String escapeLinkText(String key) {
        return key.replace("&", "&amp;").replace("<", "&lt;").replace(">", "&gt;");
    }

    public void sendNotification(@NotNull Notification notification)
    {

        String message = (notification instanceof Notification.HtmlImContentProvidingNotification)
                ? ((Notification.HtmlImContentProvidingNotification) notification).getHtmlImContent()
                : notification.getIMContent();

        if (!StringUtils.isEmpty(message))
        {

            try
            {
                HttpPost method = setupPostMethod();

                String color = COLOR_UNKNOWN_STATE;

                if (resultsSummary != null) {

                    color = getMessageColor(resultsSummary);

                    Set<Author> authors = resultsSummary.getUniqueAuthors();
                    if (!authors.isEmpty())
                    {
                        message += " Responsible Users: ";

                        ArrayList<String> usernames = new ArrayList<String>();

                        for (Author author: authors)
                        {
                            usernames.add(author.getFullName());
                        }

                        message += String.join(", ", usernames);
                    }
                } else if (deploymentResult != null) {
                    color = getMessageColor(deploymentResult);
                }

                JSONObject attachments = new JSONObject();
                JSONObject object = new JSONObject();
                try {
                    object.put("fallback", fallbackMessage(message));
                    object.put("text", textMessage(message));
                    object.put("color", color);
                    attachments.put("attachments", new JSONArray().put(object));
                    attachments.put("username", from);
                    if (StringUtils.isNotBlank(channel)) {
                        attachments.put("channel", channel);
                    }
                    if (StringUtils.isNotBlank(iconUrl)) {
                        attachments.put("icon_url", iconUrl);
                    }
                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(1);
                    nameValuePairs.add(new BasicNameValuePair("payload", attachments.toString()));
                    method.setEntity(new UrlEncodedFormEntity(nameValuePairs, "UTF-8"));

                } catch (JSONException e) {
                    log.error("JSON construction error :" + e.getMessage(), e);
                }
                catch (UnsupportedEncodingException e) {
                    log.error("Unsupported Encoding Exception :" + e.getMessage(), e);
                }
                try {
                    log.debug(method.getURI().toString());
                    log.debug(method.getEntity().toString());
                    client.execute(method);
                } catch (IOException e) {
                    log.error("Error using Slack API: " + e.getMessage(), e);
                }
            }
            catch(URISyntaxException e)
            {
                log.error("Error parsing webhook url: " + e.getMessage(), e);

            }
        }
    }

    private String getMessageColor(ResultsSummary result)
    {
        if (result.getBuildState() == BuildState.FAILED)
        {
            return COLOR_FAILED;
        }
        else if (result.getBuildState() == BuildState.SUCCESS)
        {
            return COLOR_SUCCESSFUL;
        }
        else if (LifeCycleState.isActive(result.getLifeCycleState()))
        {
            return COLOR_IN_PROGRESS;
        }

        return COLOR_UNKNOWN_STATE;
    }

    private String getMessageColor(DeploymentResult deploymentResult)
    {
        if (deploymentResult.getDeploymentState() == BuildState.FAILED)
        {
            return COLOR_FAILED;
        }
        else if (deploymentResult.getDeploymentState() == BuildState.SUCCESS)
        {
            return COLOR_SUCCESSFUL;
        }
        else if (LifeCycleState.isActive(deploymentResult.getLifeCycleState()))
        {
            return COLOR_IN_PROGRESS;
        }

        return COLOR_UNKNOWN_STATE;
    }

    private HttpPost setupPostMethod() throws URISyntaxException
    {
        return new HttpPost(new URI(webhookUrl));
    }

}
